﻿using UnityEngine;
using System.Collections;

public class UIController : MonoBehaviour {

    MicInput mic;


	// Use this for initialization
	void Start () {
        mic = GameObject.Find("Mic").GetComponent<MicInput>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void OnStartClick()
    {
        mic.StartRecording();
    }

    public void OnStopClick()
    {
        mic.StopRecording();
    }

    public void OnPlayClick()
    {
        mic.PlayRecordedSound();
    }
}
