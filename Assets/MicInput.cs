﻿using UnityEngine;
using System.Collections;
using WebSocketSharp;
using System;
using UnityEngine.UI;
using System.Collections.Generic;

[RequireComponent(typeof(AudioSource))]
public class MicInput : MonoBehaviour {

    private AudioSource micAudio;
    

    private float[] samples;
    private int lastSample = 0;

    private WebSocket ws;

    private int FREQUENCY = 44100;

    public Text text;

    
	// Use this for initialization
	void Start () {

        ws = new WebSocket("ws://echo.websocket.org/");

        ws.OnOpen += OnOpenHandler;
        ws.OnMessage += OnMessageHandler;
        ws.OnClose += OnCloseHandler;

        ws.ConnectAsync();
        
        micAudio = GetComponent<AudioSource>();
        micAudio.clip = Microphone.Start(null, true, 10, 44100);
        micAudio.pitch = 1.5f;
        micAudio.loop = true; // Set the AudioClip to loop
        micAudio.mute = false; // Mute the sound, we don't want the player to hear it
        while (!(Microphone.GetPosition(null) > 0)) { } // Wait until the recording has started
    }


    int sendCount = 0;
    int rcvCount = 0;
	// Update is called once per frame
	void Update () {

        var watch = System.Diagnostics.Stopwatch.StartNew();
        float[] micData = GrabMicData();
        Loom.RunAsync(() => { SendMicData(micData); });
        watch.Stop();
        Debug.Log(watch.ElapsedMilliseconds);
        
        
    }

    

    public float [] GrabMicData()
    {
        int pos = Microphone.GetPosition(null);
        int diff = pos - lastSample;
        if (diff > 0)
        {
            float[] samples = new float[diff * micAudio.clip.channels];
            micAudio.clip.GetData(samples, lastSample);

            lastSample = pos;
            return samples;

            
        }

        return null;
        //lastSample = pos;
    }

    public void SendMicData(float [] samples)
    {
        if (samples == null)
            return;

        var byteArray = new byte[samples.Length * 4];
        Buffer.BlockCopy(samples, 0, byteArray, 0, byteArray.Length);




        if (ws != null && ws.IsConnected)
        {
            Debug.Log("sending data of size " + byteArray.Length);
            PrintByteArray("Sending : " + sendCount + " : ", byteArray);

            ws.Send(byteArray);
            sendCount++;
        }
    }

    public void PrintByteArray(string tag, byte [] bArr)
    {
        string s = tag;

        for (int i = 0; i < 5; i++)
            s += (int)bArr[i] + " ";

        Debug.Log(s);
    }

    public byte[] ToByteArray(float[] floatArray)
    {
        int len = floatArray.Length * 4;
        byte[] byteArray = new byte[len];
        int pos = 0;
        foreach (float f in floatArray)
        {
            byte[] data = System.BitConverter.GetBytes(f);
            System.Array.Copy(data, 0, byteArray, pos, 4);
            pos += 4;
        }
        return byteArray;
    }

    public float[] ToFloatArray(byte[] byteArray)
    {
        int len = byteArray.Length / 4;
        float[] floatArray = new float[len];
        for (int i = 0; i < byteArray.Length; i += 4)
        {
            floatArray[i / 4] = System.BitConverter.ToSingle(byteArray, i);
        }
        return floatArray;
    }

    //void OnAudioFilterRead(float[] data, int channels)
    //{
    //    Debug.Log("here");

    //    var byteArray = new byte[data.Length * 4];
    //    Buffer.BlockCopy(data, 0, byteArray, 0, byteArray.Length);

    //    if (ws.IsConnected)
    //    {
    //        Debug.Log("sending");
    //        ws.Send(byteArray);
    //    }


    //    //for (int i = 0; i < data.Length; i++)
    //    //{
    //    //    data[i] = data[i] * 2;
    //    //}

    //    //float avgl, avgr;
    //    //int m_sampleRate = 128;
    //    //int m_sampleDepth = 4;

    //    //for (int i = 0; i < data.Length - m_sampleRate * channels; i += m_sampleRate * channels)
    //    //{
    //    //    avgl = 0.0f;
    //    //    avgr = 0.0f;

    //    //    for (int j = 0; j < m_sampleRate * channels; j += channels)
    //    //    {
    //    //        avgl += data[i + j];

    //    //        if (channels > 1)
    //    //        {
    //    //            avgr += data[i + j + 1];
    //    //        }
    //    //    }

    //    //    avgl /= (float)m_sampleRate;
    //    //    avgr /= (float)m_sampleRate;

    //    //    avgl *= m_sampleDepth;
    //    //    avgl = Mathf.Round(avgl);
    //    //    avgl /= m_sampleDepth;

    //    //    avgr *= m_sampleDepth;
    //    //    avgr = Mathf.Round(avgr);
    //    //    avgr /= m_sampleDepth;

    //    //    for (int j = 0; j < m_sampleRate * channels; j += channels)
    //    //    {
    //    //        data[i + j] = avgl;
    //    //        data[i + j + 1] = avgr;
    //    //    }
    //    //}
    //}

    public void StartRecording()
    {
        if (micAudio.isPlaying)
            micAudio.Stop();

        if (!Microphone.IsRecording(null))
        {
            
            //Start recording and store the audio captured from the microphone at the AudioClip in the AudioSource  
            micAudio.clip = Microphone.Start(null, true, 20, 44100);
            
        }
    }

    public void StopRecording()
    {
        Microphone.End(null); //Stop the audio recording 
    }

    public void PlayRecordedSound()
    {
        micAudio.Play();
    }

    private void OnOpenHandler(object sender, System.EventArgs e)
    {
        Debug.Log("WebSocket connected!");

        
        //Thread.Sleep(3000);

        //byte[] b = new byte[1024];
        //for (int i = 0; i < 1024; i++)
        //{
        //    b[i] = (byte)127;
        //}

        //var watch = System.Diagnostics.Stopwatch.StartNew();
        //// the code that you want to measure comes here

        //ws.Send(b);
        //watch.Stop();
        //var elapsedMs = watch.ElapsedMilliseconds;
        //Debug.Log("Took " + elapsedMs);

        //ws.SendAsync("This WebSockets stuff is a breeze!", OnSendComplete);

    }

    public IEnumerator SetText(string data)
    {
        text.text = data;
        return null;
    }

    private void OnMessageHandler(object sender, MessageEventArgs e)
    {

        Debug.Log(e.Data);
        Debug.Log("Echo data size " + e.RawData.Length);

        PrintByteArray("Rcv " + rcvCount + " : " , e.RawData);
        rcvCount++;

        Loom.QueueOnMainThread(() =>
        {
            text.text = e.Data;
        });
        //Debug.Log("WebSocket server said: " + e.Data);

        //ExecuteOnMainThread.Enqueue(() => {
        //    StartCoroutine(SetText(e.Data)); //, e.Data);
        //});
        ////-See more at: http://blog.kibotu.net/unity-2/unity-start-coroutines-main-thread-anything-else-matter#sthash.sOMLPQhm.dpuf

        //UnityThreadHelper.Dispatcher.Dispatch(() =>
        //{
        //    text.text = e.Data;

        //});

        //((Text)GameObject.Find("RText")).text = e.Data;
        //Thread.Sleep(3000);
        //ws.CloseAsync();
    }

    private void OnCloseHandler(object sender, CloseEventArgs e)
    {
        Debug.Log("WebSocket closed with reason: " + e.Reason);
    }

    private void OnSendComplete(bool success)
    {
        Debug.Log("Message sent successfully? " + success);
    }
}
